using System.Collections.Generic;
using UnityEngine;

namespace OtusHomeWork
{
    public abstract class PathFinder
    {
        protected Field Field;

        public abstract List<Cell> GetPath();
        protected abstract void CalcCellParams(Cell prevCell, Cell cell);

        protected PathFinder(Field field)
        {
            Field = field;
        }

        protected HashSet<Cell> GetNeighbours(Cell cell)
        {
            int expectedCountNeighbours = 8;
            HashSet<Cell> result = new HashSet<Cell>(expectedCountNeighbours);

            var currentCellPos = cell.Pos;
            bool UntilTopBorder(Vector2Int pos) => pos.y < Field.RowCount;
            bool UntilBottomBorder(Vector2Int pos) => pos.y > 0;
            bool UntilLeftBorder(Vector2Int pos) => pos.x > 0;
            bool UntilRightBorder(Vector2Int pos) => pos.x < Field.ColumnCount;

            if (UntilLeftBorder(currentCellPos) && UntilBottomBorder(currentCellPos))
                result.Add(GetNeighbour(cell, -1, -1));
            if (UntilLeftBorder(currentCellPos))
                result.Add(GetNeighbour(cell, -1, 0));
            if (UntilLeftBorder(currentCellPos) && UntilTopBorder(currentCellPos))
                result.Add(GetNeighbour(cell, -1, 1));
            if (UntilTopBorder(currentCellPos))
                result.Add(GetNeighbour(cell, 0, 1));
            if (UntilRightBorder(currentCellPos) && UntilTopBorder(currentCellPos))
                result.Add(GetNeighbour(cell, 1, 1));
            if (UntilRightBorder(currentCellPos))
                result.Add(GetNeighbour(cell, 1, 0));
            if (UntilRightBorder(currentCellPos) && UntilBottomBorder(currentCellPos))
                result.Add(GetNeighbour(cell, 1, -1));
            if (UntilBottomBorder(currentCellPos))
                result.Add(GetNeighbour(cell, 0, -1));

            return result;
        }

        private Cell GetNeighbour(Cell cell, int dx, int dy)
        {
            Cell neighbour = Field.Cells[cell.Pos.y + dy, cell.Pos.x + dx];
            CalcCellParams(cell, neighbour);
            return neighbour;
        }
    }
}