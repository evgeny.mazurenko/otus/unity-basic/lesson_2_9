using System;
using System.Collections.Generic;
using UnityEngine;

namespace OtusHomeWork
{
    /**
     * Определение расстояния по прямой без учета весов проходимости ячеек.
     */
    public sealed class StraightPathFinder : PathFinder
    {
        public StraightPathFinder(Field field) : base(field)
        {
        }

        public override List<Cell> GetPath()
        {
            List<Cell> result = new List<Cell>();

            Cell currentCell = Field.StartCell;
            currentCell.Cost = 0;

            result.Add(currentCell);
            while (!currentCell.Equals(Field.EndCell))
            {
                currentCell = GetNextCell(GetNeighbours(currentCell));
                result.Add(currentCell);
            }
            
            return result;
        }

        private Cell GetNextCell(HashSet<Cell> neighbourCells)
        {
            Cell nextCell = null;
            foreach (var cell in neighbourCells)
            {
                if (nextCell == null || cell.Heuristics <= nextCell.Heuristics)
                {
                    nextCell = cell;
                }
            }

            return nextCell;
        }

        protected override void CalcCellParams(Cell prevCell, Cell cell)
        {
            // Упрощенное евклидово расстояние. Опускаю извлечение корня, так как интересует только сравнение значений, а не их точность
            cell.Heuristics = (int)(Mathf.Pow(Field.EndCell.Pos.x - cell.Pos.x, 2) +
                                    Mathf.Pow(Field.EndCell.Pos.y - cell.Pos.y, 2));
            
        }
    }
}