using System.Collections.Generic;
using UnityEngine;

namespace OtusHomeWork
{
    /**
     * Определение расстояния по алгоритму A*
     */
    public sealed class AStarPathFinder : PathFinder
    {
        private const int CostToOrthogonalNeighbours = 10;
        private const int CostToDiagonalNeighbours = 14;

        private HashSet<Cell> _closedCells = new ();
        private HashSet<Cell> _openCells = new();

        public AStarPathFinder(Field field) : base(field)
        {
        }

        public override List<Cell> GetPath()
        {
            FormClosedCells();
            return CalcReversePath();
        }

        private void FormClosedCells()
        {
            Cell currentCell = Field.StartCell;
            currentCell.Cost = 0;
            
            _closedCells.Add(currentCell);
            while (currentCell != null && !currentCell.Equals(Field.EndCell))
            {
                currentCell = GetNextCell(currentCell);
                _closedCells.Add(currentCell);
            }
        }

        private Cell GetNextCell(Cell cell)
        {
            HashSet<Cell> neighbourCells = GetNeighbours(cell);
            if (neighbourCells.Contains(Field.EndCell))
            {
                return Field.EndCell;
            }
            
            neighbourCells.ExceptWith(_closedCells);
            _openCells.UnionWith(neighbourCells);

            Cell nextCell = null;
            foreach (var openCell in _openCells)
            {
                if (nextCell == null || nextCell.Weight > openCell.Weight)
                {
                    nextCell = openCell;
                }
            }
            
            _openCells.Remove(nextCell);
            return nextCell;
        }
        
        private List<Cell> CalcReversePath()
        {
            List<Cell> path = new();
            
            Cell currentCell = Field.EndCell;
            path.Add(currentCell);
            
            while (!currentCell.Equals(Field.StartCell))
            {
                currentCell = currentCell.PrevCell;
                path.Add(currentCell);
            }
            
            return path;
        }

        protected override void CalcCellParams(Cell prevCell, Cell cell)
        {
            //Так как допускается расстояние по диагонали определяю евристическое расстояние Чебышева.
            //Для минимизации обратных переходов ввожу половину коэффициента стоимости перехода по прямой.
            cell.Heuristics = Mathf.Max(
                Mathf.Abs(Field.EndCell.Pos.x - cell.Pos.x),
                Mathf.Abs(Field.EndCell.Pos.y - cell.Pos.y)
            ) * CostToOrthogonalNeighbours / 2;
            
            int costToNext = prevCell.Cost + CalcCostToNext(prevCell.Pos.x - cell.Pos.x, prevCell.Pos.y - cell.Pos.y);
            if (NeedSetCost(cell, costToNext))
            {
                cell.Cost = costToNext;
                cell.PrevCell = prevCell;
            }
        }

        private int CalcCostToNext(int dx, int dy)
        {
            return Mathf.Abs(dx) + Mathf.Abs(dy) == 2
                ? CostToDiagonalNeighbours
                : CostToOrthogonalNeighbours;
        }

        private bool NeedSetCost(Cell cell, int costToNext)
        {
            return !_closedCells.Contains(cell) && (cell.Cost > costToNext || cell.Cost == 0);
        }
    }
}