using System;
using UnityEngine;
using OtusHomeWork;
using OtusHomeWork.Rendering;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public sealed class FieldController : MonoBehaviour
{
    public const float CellSize = 1f;
    
    private const int RowCount = 6;
    private const int ColumnCount = 12;
    private bool useAStarPathFinder = true;

    [FormerlySerializedAs("gridRenderable")] [SerializeField] private GizmosFieldRenderable fieldRenderable;

    private void Start()
    {
        StartMainLoop();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartMainLoop();
        } else if (Input.GetKeyDown(KeyCode.Tab))
        {
            useAStarPathFinder = !useAStarPathFinder;
        }
    }

    private void StartMainLoop()
    {
        Field field = InitField();

        if (!field.StartCell.Equals(field.EndCell))
        {
            PathFinder pathFinder = CreatePathFinder(field);
            field.Path = pathFinder.GetPath();
        }
        fieldRenderable.DrawGrid(field);
    }

    private Field InitField()
    {
        Cell[,] cells = FieldGenerator.GenerateCells(RowCount, ColumnCount);
        Cell startCell = GetRandomCell(cells);
        Cell endCell = GetRandomCell(cells);
        return new Field(cells, startCell, endCell);
    }

    private Cell GetRandomCell(Cell[,] cells)
    {
        var idx = new Vector2Int(Random.Range(0, ColumnCount), Random.Range(0, RowCount));
        return cells[idx.y, idx.x];
    }

    private PathFinder CreatePathFinder(Field field)
    {
        if (useAStarPathFinder)
        {
            return new AStarPathFinder(field);
        }
        else
        {
            return new StraightPathFinder(field);
        }
    }
}