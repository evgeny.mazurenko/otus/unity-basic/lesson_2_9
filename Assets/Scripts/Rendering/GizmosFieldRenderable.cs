using UnityEngine;

namespace OtusHomeWork.Rendering
{
    /**
     * Класс отрисовки ячеек и прочих графических элементов.
     *
     * Необходимо, чтобы были активированы Gizmos в окнах Scene и View
     */
    public sealed class GizmosFieldRenderable : MonoBehaviour
    {
        public Field Field { get; private set; }

        public Vector3 Pos => transform.position;
        public float CellSize => FieldController.CellSize;

        public void DrawGrid(Field field)
        {
            Field = field;
        }

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying || Field == null)
                return;
            
            for (int j = 0; j <= Field.RowCount; j++)
            {
                for (int i = 0; i <= Field.ColumnCount; i++)
                {
                    DrawCell(Field.Cells[j, i]);
                }

                DrawLine(0, j * CellSize, (Field.ColumnCount + 1) * CellSize, j * CellSize);
            }

            for (int i = 0; i <= Field.ColumnCount; i++)
            {
                DrawLine(i * CellSize, 0, i * CellSize, (Field.RowCount + 1) * CellSize);
            }
            
            DrawStartCell();
            DrawEndCell();
            DrawPath();
        }

        private void DrawStartCell()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawCube(
                Pos + new Vector3(Field.StartCell.Pos.x + CellSize / 2, Field.StartCell.Pos.y + CellSize / 2),
                new Vector2(CellSize / 2, CellSize / 2)
            );
        }

        private void DrawEndCell()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(
                Pos + new Vector3(Field.EndCell.Pos.x, Field.EndCell.Pos.y + CellSize),
                Pos + new Vector3(Field.EndCell.Pos.x + CellSize, Field.EndCell.Pos.y)
            );
            Gizmos.DrawLine(
                Pos + new Vector3(Field.EndCell.Pos.x, Field.EndCell.Pos.y),
                Pos + new Vector3(Field.EndCell.Pos.x + CellSize, Field.EndCell.Pos.y + CellSize)
            );
        }

        private void DrawCell(Cell cell)
        {
            float colorWeightComponent = cell.Passability / 255f;
            Gizmos.color = new Color(1f - colorWeightComponent, 1f - colorWeightComponent, 1f - colorWeightComponent);
            Gizmos.DrawCube(
                Pos + new Vector3(cell.Pos.x + CellSize / 2, cell.Pos.y + CellSize / 2),
                new Vector2(CellSize, CellSize)
            );
        }

        private void DrawLine(float startX, float startY, float endX, float endY)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(
                Pos + new Vector3(startX, startY),
                Pos + new Vector3(endX, endY)
            );
        }

        private void DrawPath()
        {
            if (Field.Path != null && Field.Path.Count > 0)
            {
                Gizmos.color = Color.red;
                for (int i = 1; i < Field.Path.Count; i++)
                {
                    var firstPos = Field.Path[i - 1].Pos;
                    var secondPos = Field.Path[i].Pos;
                    Gizmos.DrawLine(
                        Pos + new Vector3(CellSize / 2 + firstPos.x * CellSize, CellSize / 2 + firstPos.y * CellSize),
                        Pos + new Vector3(CellSize / 2 + secondPos.x * CellSize, CellSize / 2 + secondPos.y * CellSize)
                    );
                }
            }
        }
    }
}