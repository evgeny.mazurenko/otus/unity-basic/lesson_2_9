using UnityEngine;

namespace OtusHomeWork
{
    public sealed class Cell
    {
        //Проходимость ячейки
        public int Passability { get; private set; }

        //Накопленная стоимость перехода от начала
        public int Cost { get; set; }

        //Величина евристики
        public int Heuristics { get; set; }

        //Итоговый вес ячейки
        public int Weight => Cost + Passability + Heuristics;

        public Vector2Int Pos { get; private set; }

        public Cell PrevCell { get; set; }

        private readonly string _idx;

        public Cell(int passability, int x, int y)
        {
            Passability = passability;
            Pos = new Vector2Int(x, y);
            _idx = x + ":" + y;
        }

        public override bool Equals(object obj)
        {
            if (obj is not Cell)
            {
                return false;
            }

            return _idx.Equals(((Cell)obj)._idx);
        }

        public override int GetHashCode()
        {
            return _idx.GetHashCode();
        }
    }
}