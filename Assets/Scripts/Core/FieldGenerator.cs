using System.Collections.Generic;
using Sorting;
using UnityEngine;
using Random = System.Random;

namespace OtusHomeWork
{
    public static class FieldGenerator
    {
        //Сортировка величин проходимости ячейки по убыванию. Смысла в этом действии особого нет
        private static readonly int[] Passabilities = CountingNumberSorter.Sort(new int[]{64,10,10,255,10,64,10});
        
        private static readonly Random Random = new();
        
        public static Cell[,] GenerateCells(int rowCount, int columnCount)
        {
            var cells = new Cell[rowCount, columnCount];

            for (int j = 0; j < rowCount; j++)
            {
                for (int i = 0; i < columnCount; i++)
                {
                    int passabilityIdx = Passabilities[Random.Next(0, Passabilities.Length)];
                    cells[j, i] = new Cell(passabilityIdx, i, j);
                }
            }
            return cells;
        }
    }
}