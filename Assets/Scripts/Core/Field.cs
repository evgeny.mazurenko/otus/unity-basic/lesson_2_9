using System.Collections.Generic;

namespace OtusHomeWork
{
    /**
     * Абстрацкия над прямоугольным игровым полем
     */
    public sealed class Field
    {
        public Cell[,] Cells { get; private set; }
        public Cell StartCell { get; private set; }
        public Cell EndCell { get; private set; }
        public int RowCount { get; private set; }
        public int ColumnCount { get; private set; }
        public List<Cell> Path { get; set; }

        public Field(Cell[,] cells, Cell startCell, Cell endCell)
        {
            Cells = cells;
            StartCell = startCell;
            EndCell = endCell;
            RowCount = Cells.GetUpperBound(0);
            ColumnCount = Cells.GetUpperBound(1);
        }
    }
}