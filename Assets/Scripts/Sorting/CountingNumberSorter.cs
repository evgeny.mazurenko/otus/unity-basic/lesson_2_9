using UnityEngine;

namespace Sorting
{
    /**
     * Алгоритм сортировки подсчетом, применяется только для целых положительных чисел.
     * 3 прохода по размерности исходного массива (n) и один проход по размерности массива-счетчика (k)
     */
    public static class CountingNumberSorter
    {
        public static int[] Sort(int[] sourceArr)
        {
            PrintArr("Первоначальный массив", sourceArr);
            
            int[] sortedArr = new int[sourceArr.Length];
            int maxSourceItem = Mathf.Max(sourceArr);
            int[] countingArr = new int[maxSourceItem+1];

            foreach (var sourceItem in sourceArr)
            {
                countingArr[sourceItem]++;
            }

            int i = 0;
            for (int j = countingArr.Length-1; j >= 0; j-- )
            {
                for (int k = 0; k < countingArr[j]; k++)
                {
                    sortedArr[i + k] = j;
                }
                i += countingArr[j];
            }
            
            PrintArr("Отсортированный массив", sortedArr);
            return sortedArr;
        }

        private static void PrintArr(string text, int[] arr)
        {
            Debug.Log($"{text}: {string.Join(",",arr)}");
        }
        
    }
}