using OtusHomeWork.Rendering;
using UnityEditor;
using UnityEngine;

namespace OtusHomeWork
{
    /**
     * Класс отрисовки числовых значений ячеек.
     */
    [CustomEditor(typeof(GizmosFieldRenderable))]
    public class HandlesFieldRenderableEditor : Editor
    {
        private GizmosFieldRenderable _renderable;
        
        void OnSceneGUI()
        {
            if (!Application.isPlaying)
                return;

            if (_renderable == null)
                _renderable = (GizmosFieldRenderable)target;
            
            Field field = _renderable.Field;
            if (field != null)
            {
                for (int j = 0; j <= field.RowCount; j++)
                {
                    for (int i = 0; i <= field.ColumnCount; i++)
                    {
                        DrawCellTexts(field.Cells[j, i]);
                    }
                }
            }
        }

        private void DrawCellTexts(Cell cell)
        {
            if (cell.Heuristics != 0)
            {
                DrawHeuristics(cell);
                DrawCost(cell);
                DrawWeight(cell);
            }
        }

        private void DrawHeuristics(Cell cell)
        {
            Vector3 pos = new Vector3(cell.Pos.x + _renderable.CellSize / 8, cell.Pos.y + 3 * _renderable.CellSize / 4);
            DrawText(Color.red, _renderable.Pos + pos, "H:" + cell.Heuristics);
        }

        private void DrawCost(Cell cell)
        {
            Vector3 pos = new Vector3(cell.Pos.x + _renderable.CellSize / 8, cell.Pos.y + 2 * _renderable.CellSize / 4);
            DrawText(Color.blue, _renderable.Pos + pos, "C:" + cell.Cost);
        }
        
        private void DrawWeight(Cell cell)
        {
            Vector3 pos = new Vector3(cell.Pos.x + _renderable.CellSize / 8, cell.Pos.y + 1 * _renderable.CellSize / 4);
            DrawText(Color.magenta, _renderable.Pos + pos, "W:" + cell.Weight);
        }

        private void DrawText(Color color, Vector3 pos, string text)
        {
            GUI.color = color;
            Handles.Label(pos, text);
        }
    }
}